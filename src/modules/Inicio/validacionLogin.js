import React from 'react'
import {ContenedorMensaje,Mensaje} from './../../styles/LoginStyle'


const validacion = (props) =>{



    return(
        props.success?(<ContenedorMensaje>
            <Mensaje success>{props.mensaje}</Mensaje>
        </ContenedorMensaje>):
        <ContenedorMensaje  >
        <Mensaje>{props.mensaje}</Mensaje>
    </ContenedorMensaje>
    )
}


export default validacion