import {useState,useEffect} from 'react'
import { useSelector } from 'react-redux'
import Loader from 'react-loader-spinner'
import React from 'react'
import Marquee from "./Marquee";
import {Container,MenuSup,MenuInf,ContDer,ContIzq,ItemsContIzq,Item,Enlace} from './../../styles/PanelStyle'


const Panel=()=>{

    const [user,setUser]=useState({})
    const [isUser,setIsUser] = useState(false)
    

    useEffect(async()=>{
        let data = await userData
        console.log("data en panel ",data);
        setUser(data)
        setIsUser(data.isAuth)
    },[userData])

    const userData = useSelector(state=>state.login)



    return(
        <div>
            {isUser?(
                <Container>
                    <MenuSup>
                        <Marquee texto_cinta={"Bienvenid@  "+user.usuario.primer_nombre + " " + user.usuario.apellido_paterno}></Marquee>
                    </MenuSup>
                    <MenuInf>
                        <ContIzq>
                            <ItemsContIzq>
                                <Item>
                                    <Enlace href="#">Productos</Enlace>
                                </Item>
                                <Item>
                                    <Enlace href="#">Clientes</Enlace>
                                </Item>
                                <Item>
                                    <Enlace href="#">Ventas</Enlace>
                                </Item>
                                <Item>
                                    <Enlace href="#">Reportes</Enlace>
                                </Item>
                            
                            </ItemsContIzq>
                        </ContIzq>
                        <ContDer/>
                    </MenuInf>
    
                </Container>
            ):[<Loader
                    type="Oval"
                    color="#00BFFF"
                    height={100}
                    width={100}
                />]}

        </div>
    )

}


export default Panel