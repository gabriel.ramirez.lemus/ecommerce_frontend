import React from 'react'
import Marquee from "react-fast-marquee";
const CintaMarquee=(props)=>{


    return <Marquee speed={150} gradient={false} style={{fontSize:"23px",overflow:"hidden"}}>
        {props.texto_cinta}
    </Marquee>
}


export default CintaMarquee