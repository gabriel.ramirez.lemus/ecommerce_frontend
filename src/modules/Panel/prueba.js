import React , {useState,useEffect} from 'react'
import {Container,MenuSup,MenuInf,ContDer,ContIzq,ItemsContIzq,Item,Enlace} from './../../styles/PanelStyle'
import Marquee from './Marquee'


const Prueba=()=>{

    const [user,setUser] = useState({})



    return(
        <Container>
            <MenuSup>
                <Marquee texto_cinta="TEXTO DE PRUEBA"></Marquee>
            </MenuSup>
            <MenuInf>
                <ContIzq>
                    <ItemsContIzq>
                        <Item>
                            <Enlace href="#">Productos</Enlace>
                        </Item>
                        <Item>
                            <Enlace href="#">Clientes</Enlace>
                        </Item>
                        <Item>
                            <Enlace href="#">Ventas</Enlace>
                        </Item>
                        <Item>
                            <Enlace href="#">Reportes</Enlace>
                        </Item>
                        
                    </ItemsContIzq>
                </ContIzq>
                <ContDer/>
            </MenuInf>

        </Container>
    )
}


export default Prueba