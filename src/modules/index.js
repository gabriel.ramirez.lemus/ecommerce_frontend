import React,{useState,useEffect} from 'react'
import { useDispatch,useSelector } from 'react-redux'
import {aumentar,disminuir,ingresar} from './../redux/actions/Login'
import Validacion from './../modules/Inicio/validacionLogin'
import {useHistory,Redirect} from 'react-router-dom'




import {Contenedor,Login,Boton,Title,InputUsuario,InputPassword, LabelNombreUsuario, LabelPassword,LabelCount} from '../styles/LoginStyle'



const App=()=>{
    let history = useHistory()

    const [count, setCount] = useState(0);
    const [user,setUser] = useState({})
    const [username,setUsername] = useState("")
    const [pass,setPass] = useState("")
    const [showLogin,setShowLogin] = useState(false)
    const dispatch= useDispatch()
    const counter = useSelector(state=>state.count)
    const userData=useSelector(state=>state.login)


    useEffect(async()=>{
        let data=await userData
        console.log(data);  
        setUser(data)
        
    },[userData])

    const ingreso=()=>{
        
        dispatch(ingresar(username,pass))
        if(!user.isAuth){
            setShowLogin(true)
            setTimeout(()=>{
                setShowLogin(false)
            },3000)
        }
        
       
        
    }

    
    

    return(
        <Contenedor>
            <Title>
                Login
            </Title>
            <Login>
                <LabelNombreUsuario>Nombre usuario</LabelNombreUsuario>
                <InputUsuario onChange={(event)=>setUsername(event.target.value)}></InputUsuario>
                <LabelPassword>Contraseña</LabelPassword>
                <InputPassword onChange={(event)=>setPass(event.target.value)}></InputPassword>
                <Boton  onClick={()=>ingreso()}>
                    Ingresar
                </Boton>
                {/* <Boton  onClick={()=>dispatch(aumentar())}>
                    Aumentar
                </Boton>
                <Boton  onClick={()=>dispatch(disminuir())}>
                    Disminuir
                </Boton> */}
               
                
            </Login>
            {/* <LabelCount>{counter}</LabelCount> */}
            {/*showLogin?user.isAuth?<Validacion success={true} mensaje={user.mensaje}/>:<Validacion success={false} mensaje={user.mensaje}/>:""*/}
            {showLogin?!user.isAuth?(<Validacion success={false} mensaje={user.mensaje}/>):(<Redirect to="/portal" />):""}
            
        
        </Contenedor>
    )
}


export default App