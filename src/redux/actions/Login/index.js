import {AUMENTAR,DISMINUIR,INGRESAR} from './types'

export const aumentar=()=>({
    type:AUMENTAR
})

export const disminuir=()=>({
    type:DISMINUIR
})

export const ingresar=(user,pass)=>({
    type:INGRESAR,
    payload:{username:user,password:pass}
})