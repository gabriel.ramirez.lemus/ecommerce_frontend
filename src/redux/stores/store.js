
import {createStore,combineReducers} from 'redux'
import {countReducer,loginReducer} from './../reducers/LoginReducers'

const stores={
    count:countReducer,
    login:loginReducer
}

export const store= createStore(combineReducers(stores),window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__())