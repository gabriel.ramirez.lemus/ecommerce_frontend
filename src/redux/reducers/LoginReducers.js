import {AUMENTAR,DISMINUIR,INGRESAR} from './../actions/Login/types'
import {loginUsuario} from './../../../apis/Login'

const countState=0

const countReducer =(state=countState,action)=>{
    switch (action.type) {
        case AUMENTAR:
            
            return state+1
            
        case DISMINUIR:
            return state-1
    
        default:
            return state;
    }
    

}

let loginState=
{
    usuario:{},
    mensaje:"",
    isAuth:false

}

const loginReducer=async(state=loginState,action)=>{
    
    switch(action.type){
        case INGRESAR:
            console.log("ingresar");
            //let isAuth= validateUser()     funcion api que ese comunica con el endpoint
            let user = await loginUsuario(action.payload.username,action.payload.password)
            console.log(user);
            let userData={
                usuario:user,
                isAuth:true
            }
            if(user.status==2){
                console.log("logueo exitoso");
                
                
                return {...state,usuario:userData.usuario.usuario,isAuth:userData.isAuth,mensaje:userData.usuario.mensaje}
            }else{
                console.log("error ");
                return {...state,usuario:{},isAuth:false,mensaje:userData.usuario.mensaje}
            }
        default:
            
            return state
            
    }
}



export {countReducer,loginReducer}