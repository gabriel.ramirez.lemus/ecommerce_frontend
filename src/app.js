import ReactDOM from 'react-dom'
import React from 'react'
import App from "./modules/index"
import Panel from './modules/Panel'
import Prueba from './modules/Panel/prueba'
import Tienda from './modules/Tienda'
import {Provider} from 'react-redux'
import {store} from './redux/stores/store'

import {
        BrowserRouter as Router,
        Switch,
        Route,
        Link
} from 'react-router-dom'

const elem=document.getElementById("app")



ReactDOM.render(
        <Provider store={store}>
            <Router>
                <Switch>
                    <Route path="/portal"> 
                        <Panel/>
                    </Route>
                    <Route path="/prueba"> 
                        <Prueba></Prueba>
                    </Route>
                    <Route path="/login"> 
                        <App></App>
                    </Route>
                    <Route path="/"> 
                        <Tienda></Tienda>
                    </Route>
                    
                </Switch>
            </Router>
            
                
        </Provider>
    
,elem)