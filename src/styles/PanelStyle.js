import styled from 'styled-components'


const Container=styled.div`
    display:flex;
    flex-direction:column;
    height:100vh;
    background-color:#952ca5;
`

const  MenuSup=styled.div`
    display:flex;
    height:3%;
    background-color:grey;
`

const MenuInf=styled.div`
    display:flex;
    height:97%;
`
const ContIzq=styled.div`
    display:flex;
    flex-direction:column;
    width:10%;
    
`
const ItemsContIzq=styled.div`
    display:flex;
    flex-direction:column;
    justify-content:space-around;
    flex:0.25;
`

const Item=styled.div`
    display:flex;
    border-style:solid;
    border-color:black;
    background-color:grey;
`

const Enlace = styled.a`
    font-size=2rem;
    width:100%;
    height:100%;
    text-align:center;
    padding: 2% 0 ;
    text-decoration:none;
    color:black;
`

const ContDer=styled.div`
    display:flex;
`


export {Container,MenuSup,MenuInf,ContIzq,ContDer,ItemsContIzq,Item,Enlace}