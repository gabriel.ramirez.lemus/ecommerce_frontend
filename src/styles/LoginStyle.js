import styled from 'styled-components'



const Title=styled.h1`
    text-align:center;
    font-size:2rem;
    color:palevioletred;

`

const Contenedor=styled.div`
    display:flex;
    background-color: black;
    flex-direction: column;
    height:100vh;
    align-items:center;
`

const Login = styled.div`
    display:flex;
    flex-direction:column;
    width:20%;
    height:50%;
    border-style:solid;
    border-color:green;
    align-items:center;
`

const Boton = styled.button
`
    display:flex
    background: ${props=>props.primary?"green":"white"};
    font-size: 1em;
    padding: 0.3rem 0;
    margin-top:4%;
    border: 2px solid black;
    border-radius: 10px;
    width:10rem;

`





const InputUsuario=styled.input.attrs(props=>({
    type:"text"
}))
`
    display:flex;
    
    

`
const InputPassword=styled.input.attrs(props=>({
    type:"password"
}))
`
    display:flex;
`

const LabelNombreUsuario=styled.label`
    display:flex;
    color:white;
    padding-top:4rem;
    padding-bottom:1rem;
`
const LabelPassword=styled.label`
    display:flex;
    color:white;
    padding-top:2rem;
    padding-bottom:1rem;

`
const LabelCount=styled.label`
    display:flex;
    color:white;
    font-size:30px;
`




// VALIDACION LOGIN


const ContenedorMensaje=styled.div`
    display:flex;
    width:35%;
    height:20%;

`

const Mensaje=styled.h2`
    color:${props=>props.success?'green':'red'};
`

export {
    Contenedor,
    Title,
    Login,
    Boton,
    InputUsuario,
    InputPassword,
    LabelNombreUsuario,
    LabelPassword,
    LabelCount,
    ContenedorMensaje,
    Mensaje
}