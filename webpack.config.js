const path = require("path")

module.exports={
    entry:["@babel/polyfill","./src/app.js"],
    output:{
        filename:"bundle.js",
        path:path.join(__dirname,"public/dist")
    },
    module:{
        rules:[
            {
                exclude:/node_modules/,
                test:/.js$/,
                use:"babel-loader"
            }
        ]
    },
    devServer: {
        static: {
            directory: path.join(__dirname, 'public'),
        },
        compress: true,
        port: 9000,
        historyApiFallback: true,
      },
}