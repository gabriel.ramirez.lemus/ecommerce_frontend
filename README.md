# Frontend Ecommerce

Esta aplicacion tiene como funcion, servir como un ecommerce de prueba utilizando servicios
para la ejecución de transacciones. 


## Tecnologías utilizada
- [Nodejs] - Entorno de ejecución multiplataforma orientado a Javascript
- [Webpack] - Paquete de modulos de Javascript
- [ReactJs] - Biblioteca de Javascript para crear interfaces de usuario utilizando [JSX]
- [Redux] - Tecnica y patron de diseño utilizado generalmente para el manejo de estados con React
- [Babel] - Transconspilador de Javascript, haciendo ECMAScript 2015+ compatible con versiones antiguas de javascript
- [Bootstrap] - Biblioteca multiplataforma o conjunto de herramientas de código abierto para diseño de sitios


## Instalacion del proyecto
```sh
npm i

```


## Empaquetar código
Para empaquetar el código usando Webpack para adaptar el codigo a version antigua a JS se ejecuta el siguiente comando 
```sh
npm run build:dev

```

## Ejecutar código

```sh
npm run dev:server

```

## Icono de repositorio
Icons made by https://www.freepik.com  from https://www.flaticon.com/ 
